from itertools import compress

from matplotlib.pyplot import close, savefig, plot, scatter
from matplotlib.pyplot import xlim, ylim, xlabel, ylabel
from scipy.io import wavfile
from seaborn import jointplot, histplot, residplot


class Trigger:
    mode = 0
    tick = 0
    peak = 0.0
    threshold = 0.05
    retrig_window = 4000
    peak_window = 150
    last_x = 0.0
    seen_x = []
    seen_xs = []

    def process(self, x):
        hit = 0.0

        # Listening
        if self.mode == 0:
            if (self.last_x + x) / 2 > self.threshold:
                self.mode = 1
        # Active
        elif self.mode == 1:
            if self.tick == self.peak_window:
                raise ValueError("No hit found")
            if (x - self.last_x) < 0:
                hit = self.last_x
                self.mode = 2
            self.seen_x.append(x)
            self.tick += 1
        # Waiting
        elif self.mode == 2:
            if self.tick == self.retrig_window:
                self.mode = 0
                self.tick = 0
                self.peak = 0.0
                self.seen_xs.append(self.seen_x)
                self.seen_x = []
            self.tick += 1

        self.last_x = x
        if hit:
            return self.tick, hit


if __name__ == "__main__":
    samplerate, data = wavfile.read("data/Many Triggers.wav")
    trigger = Trigger()

    hit_ts = []
    hit_xs = []
    hit_slopes = []
    time_to_hits = []
    for i, x in enumerate(data):
        h = trigger.process(x)
        if h:
            t1 = i - h[0]
            t2 = i
            y1 = trigger.threshold
            y2 = h[1]
            hit_ts.append(t2)
            hit_xs.append(y2)
            hit_slopes.append((y2 - y1) / (t2 - t1))
            time_to_hits.append(t2 - t1)
            plot([t1, t2], [y1, y2], c="g")

    plot(range(len(data)), data)
    scatter(x=hit_ts, y=hit_xs, c="r", marker="x")
    xlabel("Time [samples]")
    ylabel("Amplitude [unit displacement]")
    savefig("plots/all_detections")

    ylim(-0.25, 0.75)
    xlim(177_655, 177_800)
    xlabel("Time [samples]")
    ylabel("Amplitude [unit displacement]")
    savefig("plots/single_slope")

    xlim(177_500, 184_000)
    xlabel("Time [samples]")
    ylabel("Amplitude [unit displacement]")
    savefig("plots/single_detection")
    close("all")

    ax = histplot(time_to_hits)
    ax.set_xlabel("Delay [samples]")
    ax.set_ylabel("Count")
    savefig("plots/time_trigger_to_peak")
    close("all")

    ax = jointplot(x=hit_slopes, y=hit_xs)
    ax.set_axis_labels(
        "Trigger peak slope [unit displacement/samples]",
        "Trigger peak value [unit displacement]",
    )
    ax.figure.tight_layout()
    savefig("plots/amplitude_vs_slope")
    close("all")

    # average slope of data after activation
    n_samples = 3
    slope_preds = []
    for xs in trigger.seen_xs:
        deltas = []
        for i in range(1, n_samples):
            deltas.append(xs[i] - xs[i - 1])
        slope_preds.append(sum(deltas) / len(deltas))

    ax = jointplot(x=slope_preds, y=hit_xs)
    ax.set_axis_labels(
        "Estimated peak slope [unit displacement/samples]",
        "Trigger peak value [unit displacement]",
    )
    ax.figure.tight_layout()
    savefig("plots/amplitude_vs_estimated_slope")
    close("all")

    ax = jointplot(x=slope_preds, y=hit_slopes)
    ax.set_axis_labels(
        "Estimated peak slope [unit displacement/samples]",
        "Actual peak slope [unit displacement/samples]",
    )
    ax.figure.tight_layout()
    savefig("plots/slope_vs_estimated_slope")
    close("all")

    ax = residplot(x=slope_preds, y=hit_slopes, lowess=True)
    ax.set_xlabel("Estimated peak slope [unit displacement/samples]")
    ax.set_ylabel("Actual peak slope [unit displacement/samples]")
    ax.yaxis.tick_right()
    savefig("plots/slope_vs_estimated_slope_residuals")
    close("all")
